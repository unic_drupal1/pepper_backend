<?php

namespace Drupal\pepper_backend\Controller;

use Drupal\user\Controller\UserController;

/**
 * Custom Pepper UserController.
 *
 * @see https://jira.unic.com/browse/PEPPER-176
 */
class PepperUserController extends UserController {

  /**
   * Logs the current user out.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect the user to the login page instead of homepage when logging out
   */
  public function logout() {
    if ($this->currentUser()->isAuthenticated()) {
      user_logout();
    }
    return $this->redirect('user.login');
  }

}
