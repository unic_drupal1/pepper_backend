<?php

namespace Drupal\pepper_backend\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\State\StateInterface;
use Symfony\Component\HttpFoundation\Response;
use Psr\Container\ContainerInterface;

/**
 * Return hash with latest content change timestamp and drupal deploy date.
 */
class PepperVersionHashController extends ControllerBase {
  /**
   * The Drupal database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The state service provided by Drupal.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * PepperStateLatestChange constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The Drupal database connection service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service provided by Drupal.
   */
  public function __construct(Connection $database, StateInterface $state) {
    $this->database = $database;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('state')
    );
  }

  /**
   * Returns hash with latest change timestamp and drupal deploy date.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response.
   */
  public function pepperVersionHash() {
    $hash = '';
    $changeDate = $this->getLatestDrupalChange();
    $deployDate = $this->getLastReleaseDate();

    if (!empty($changeDate) && !empty($deployDate)) {
      $hash = hash('sha256', $changeDate . $deployDate);
    }
    return $this->buildResponse($hash);
  }

  /**
   * Get the latest change date from nodes, media or menu link items.
   *
   * @return string
   *   The latest change date.
   */
  public function getLatestDrupalChange() {
    // Query to get the latest change date from nodes, media or menu link items.
    $query = "SELECT MAX(changed) AS newest_date FROM (
          SELECT changed FROM node_field_data
          UNION
          SELECT changed FROM media_field_data
          UNION
          SELECT changed FROM menu_link_content_data
        ) AS combined_tables ";

    // Execute the query.
    $result = $this->database->query($query);

    // Fetch the result.
    return $result->fetchField();
  }

  /**
   * Get the date of the last release.
   *
   * @return string
   *   The date of the last release.
   */
  public function getLastReleaseDate() {
    $release_date = $this->state->get('release_date');
    if (empty($release_date)) {
      // Format date like this: Mon Mar 25 11:05:19 UTC 2024
      $release_date = date('D M d H:i:s T Y');
      $this->state->set('release_date', $release_date);
    }
    return $release_date;
  }

  /**
   * Build response.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response.
   */
  public function buildResponse($string) {
    // Check if the input string is empty.
    if (empty($string)) {
      throw new \InvalidArgumentException('Input string cannot be empty.');
    }
    $response = new Response();
    $response->headers->set('Content-Type', 'text/plain');
    $response->setContent($string);
    return $response;
  }

}
