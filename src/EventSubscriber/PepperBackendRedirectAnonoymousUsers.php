<?php

namespace Drupal\pepper_backend\EventSubscriber;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Redirect visitor to login page if user is accessing a node and is anonymous.
 */
class PepperBackendRedirectAnonoymousUsers implements EventSubscriberInterface {
  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * {@inheritDoc}
   */
  public function __construct(AccountInterface $user)  {
    $this->user = $user;
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = array('onResponse');
    return $events;
  }

  /**
   * Redirect all requests from anonymous users to login page.
   *
   * @param ResponseEvent $event
   */
  public function onResponse(ResponseEvent $event) {
    $request = $event->getRequest();
    $route = $request->get('_route');
    if ($this->user->isAnonymous() && $route == 'entity.node.canonical') {
      $loginUrl = Url::fromRoute('user.login')->toString();
      $event->setResponse(new RedirectResponse($loginUrl, 301));
    }
  }

}
