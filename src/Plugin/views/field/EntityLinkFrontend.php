<?php

namespace Drupal\pepper_backend\Plugin\views\field;

use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\views\Plugin\views\field\EntityLink;
use Drupal\views\ResultRow;

/**
 * Field handler to present a link to the frontend view of an entity.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("entity_link_frontend")
 */
class EntityLinkFrontend extends EntityLink {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $row) {
    $access = $this->checkUrlAccess($row);
    return ['#markup' => $access ? $this->renderLink($row) : ''];
  }

  /**
   * {@inheritdoc}
   */
  protected function renderLink(ResultRow $row) {
    $this->options['alter']['query'] = $this->getDestinationArray();
    return parent::renderLink($row);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultLabel() {
    return $this->t('view in frontend');
  }

  /**
   * {@inheritdoc}
   */
  protected function getUrlInfo(ResultRow $row) {
    $entity = $this->getEntity($row);
    if ($this->languageManager->isMultilingual()) {
      $entity = $this->getEntityTranslation($entity, $row);
    }
    if ($entity instanceof NodeInterface) {
      /** @var NodeInterface $node */
      $node = $entity;
      $frontend_base_url = Settings::get('pepper_preview.base_url');
      if ($frontend_base_url) {
        $path = $node->toUrl()->toString();

        /** @var Url $url */
        $url = Url::fromUri($frontend_base_url . $path)
          ->setAbsolute($this->options['absolute']);
        $url->setOptions(['attributes' => [
          'class' => ['entity-link-frontend'],
          'aria-label' => $this->t('Live Page'),
        ]]);

        return $url;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['absolute'] = ['default' => TRUE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkUrlAccess(ResultRow $row) {
    $entity = $this->getEntity($row);
    return $entity->access('view');
  }

}

