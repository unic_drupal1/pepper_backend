<?php

namespace Drupal\pepper_backend\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * PepperUserRouteSubscriber is used to overwrite RouteSubscriberBase.
 *
 * @extends RouteSubscriberBase
 */
class PepperBackendRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritDoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('user.logout')) {
      $route->setDefault('_controller', '\Drupal\pepper_backend\Controller\PepperUserController::logout');
    }
  }

}
